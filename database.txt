CREATE TABLE Invitation
(
  status CHAR(15) DEFAULT 'Active',
  idInvitation INT NOT NULL,
  PRIMARY KEY (idInvitation)
);

CREATE TABLE Meeting
(
  time TIME,
  date DATE,
  name CHAR(15) NOT NULL,
  idMeeting INT NOT NULL,
  idInvitation INT NOT NULL,
  PRIMARY KEY (idMeeting),
  FOREIGN KEY (idInvitation) REFERENCES Invitation(idInvitation)
);

CREATE TABLE Event
(
  time TIME NOT NULL,
  date DATE NOT NULL,
  name CHAR(15) NOT NULL,
  description CHAR(50) NOT NULL,
  idEvent INT NOT NULL,
  PRIMARY KEY (idEvent)
);

CREATE TABLE Employee
(
  name CHAR(15) NOT NULL,
  surname CHAR(15) NOT NULL,
  jobDescription(30) CHAR NOT NULL,
  idUser INT NOT NULL,
  idInvitation INT,
  PRIMARY KEY (idUser),
  FOREIGN KEY (idInvitation) REFERENCES Invitation(idInvitation)
);

CREATE TABLE Organization
(
  name CHAR(30) NOT NULL,
  idOrganization INT NOT NULL,
  idUser INT NOT NULL,
  PRIMARY KEY (idOrganization),
  FOREIGN KEY (idUser) REFERENCES Employee(idUser)
);

CREATE TABLE at
(
  idUser INT NOT NULL,
  idEvent INT NOT NULL,
  PRIMARY KEY (idUser, idEvent),
  FOREIGN KEY (idUser) REFERENCES Employee(idUser),
  FOREIGN KEY (idEvent) REFERENCES Event(idEvent)
);

CREATE TABLE has
(
  idUser INT NOT NULL,
  idInvitation INT NOT NULL,
  PRIMARY KEY (idUser, idInvitation),
  FOREIGN KEY (idUser) REFERENCES Employee(idUser),
  FOREIGN KEY (idInvitation) REFERENCES Invitation(idInvitation)
);

CREATE TABLE is_in
(
  idUser INT NOT NULL,
  idMeeting INT NOT NULL,
  PRIMARY KEY (idUser, idMeeting),
  FOREIGN KEY (idUser) REFERENCES Employee(idUser),
  FOREIGN KEY (idMeeting) REFERENCES Meeting(idMeeting)
);
