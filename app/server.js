const path = require('path');
const client = require('./database.js')
const bodyParser = require("body-parser");
const express = require('express');
const cors = require("cors");

const app = express();

app.use(cors());
app.use(bodyParser.json());

app.listen(3000, () => {
    console.log("Server is now listening at port 3000");
})

client.connect( err => {
    if (err) {
        console.error('connection error', err.stack)
    } else {
        console.log('connected')
    }
});

app.get('/employees/:idemployee/events', (req, res) => {
    client.query(`SELECT * FROM at NATURAL JOIN event WHERE iduser='${req.params.idemployee}';`, (err, result) => {
        if(err) {
            throw err
        }
        res.status(200).json(result.rows)
    });
    client.end;
})

app.get('/employees/:idemployee/invitations', (req, res) => {
    client.query(`SELECT * from has NATURAL JOIN invitation NATURAL JOIN meeting WHERE iduser='${req.params.idemployee}';`, (err, result) => {
        if(err) {
            throw err
        }
        res.status(200).json(result.rows)
    });
    client.end;
})

app.put('/employees/:idemployee/invitation/:idinvitation/accept', (req, res) => {
    client.query(`UPDATE invitation SET status = 'accepted' WHERE idinvitation='${req.params.idinvitation}' AND status='active';`, (err, result) => {
        if(err) {
            throw err
        }
        res.status(204).json('Invitation updated')
    });
    client.end;
})

app.put('/employees/:idemployee/invitation/:idinvitation/decline', (req, res) => {
    client.query(`UPDATE invitation SET status = 'declined' WHERE idinvitation='${req.params.idinvitation}' AND status='active';`, (err, result) => {
        if(err) {
            throw err
        }
        res.status(204).json('Invitation updated')
    });
    client.end;
})